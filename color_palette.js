"use strict";
var ColorPalette = function(c, offset_x, offset_y) {
  DragableEntity.call(this, c, offset_x, offset_y);
  this.num_cols = 2;
  this.num_rows = 8;
  this.width = 32;
  this.height = 128; 
  
  this.matrix = [["#000000","#9D9D9D","#FFFFFF","#BE2633","#E06F8B","#493C2B", "#A46422", "#EB8931"],
                 ["#F7E26B","#2F484E","#44891A","#A3CE27","#1B2632","#005784","#31A2F2","#B2DCEF"]];

  this.colour = this.matrix[0][0];
  this.colour2 = this.matrix[0][2];

  this.offset_x = offset_x;
  this.offset_y = offset_y;

}
inheritsFrom(ColorPalette, DragableEntity);

ColorPalette.prototype.draw_cells = function(ctx)
{
  for(var i=0; i<this.num_cols; i++) {
    for(var j=0; j<this.num_rows; j++) {
      ctx.fillStyle = this.matrix[i][j];
      ctx.fillRect(this.offset_x + i*this.width/this.num_cols,this.offset_y + j*this.height/this.num_rows, this.width/this.num_cols, this.height/this.num_rows);
    }
  }
}

ColorPalette.prototype.draw_selected_colors = function(ctx)
{
    ctx.fillStyle = this.colour;
    ctx.fillRect(this.offset_x, this.offset_y + this.height + this.height/this.num_rows , this.width/this.num_cols, this.height/this.num_rows);   
    ctx.fillStyle = this.colour2;
    ctx.fillRect(this.offset_x + this.height/this.num_rows, this.offset_y + this.height + this.height/this.num_rows , this.width/this.num_cols, this.height/this.num_rows);
    
    ctx.beginPath();
    ctx.strokeStyle = "#000000";   
    ctx.rect(this.offset_x, this.offset_y+ this.height + this.height/this.num_rows , this.width/this.num_cols, this.height/this.num_rows);   
    ctx.rect(this.offset_x + this.height/this.num_rows, this.offset_y+ this.height + this.height/this.num_rows , this.width/this.num_cols, this.height/this.num_rows);
    ctx.stroke();
}

ColorPalette.prototype.draw_grid = function(ctx)
{
  ctx.beginPath();
  ctx.strokeStyle = "#000000";
  for(var i=0; i<this.num_cols+1; i++) {
    ctx.moveTo(Math.floor(this.offset_x + i*this.width/this.num_cols), this.offset_y);
    ctx.lineTo(Math.floor(this.offset_x + i*this.width/this.num_cols), this.offset_y + this.height);
  }
  for(var j=0; j<this.num_rows+1; j++) {
    ctx.moveTo(this.offset_x + 0, Math.floor(this.offset_y + j*this.height/this.num_rows));
    ctx.lineTo(this.offset_x + this.width, Math.floor( this.offset_y + j*this.height/this.num_rows));
  }
  ctx.stroke();
}

ColorPalette.prototype.draw = function(ctx)
{
  
  DragableEntity.prototype.draw.call(this, ctx);
  this.draw_cells(ctx);
  this.draw_selected_colors(ctx);
  this.draw_grid(ctx);
}

ColorPalette.prototype.manage_mousemove_event = function(evt)
{
  return DragableEntity.prototype.manage_mousemove_event.call(this, evt);
}

ColorPalette.prototype.manage_mousedown_event = function(evt)
{
 
  DragableEntity.prototype.manage_mousedown_event.call(this, evt)
  if (this.mouse_down)
  {
    var i = Math.floor((this.mousePos.x - this.offset_x)/(this.width/this.num_cols));
    var j = Math.floor((this.mousePos.y - this.offset_y)/(this.height/this.num_rows));

    if (i<0 || j<0 || i>this.num_cols-1 || j>this.num_rows-1)
      return false;
    if(evt.button === LEFT_BTN ){
      this.colour = this.matrix[i][j];
    }
    else if(evt.button === RIGHT_BTN ){
      this.colour2 = this.matrix[i][j];
    }

    return true;
  }
  return false;
}

