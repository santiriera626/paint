"use strict";

var CanvasEntity = function(c, offset_x, offset_y) {

  this.c = c;
  this.offset_x = offset_x;
  this.offset_y = offset_y;
  this.color = "#0000ff";
  this.width = 50;
  this.height = 50; 

  this.mouse_down = false;

  this.mousePos = {
    x: 0,
    y: 0
  };

}

CanvasEntity.prototype.draw = function(ctx) 
{
  if (DEBUG && this.mouse_down)
    ctx.fillStyle = "#000000";
  else
    ctx.fillStyle = this.color;

  ctx.fillRect(this.offset_x, this.offset_y , this.width, this.height);    
}

CanvasEntity.prototype.manage_mousedown_event = function(evt)
{
  this.mouse_down=false;
  this.mousePos = getMousePos(this.c, evt);

  if ( this.inside_bounds(this.mousePos.x, this.mousePos.y))
  {
    console.log("mouse_down = true");
    this.mouse_down=true;
  }
  else
  {
    console.log("mouse_down = false");
  }
  
  return true;
}
CanvasEntity.prototype.manage_mouseup_event = function(evt)
{
  this.mousePos = getMousePos(this.c, evt);
  this.mouse_down = false;

  return true;
}

CanvasEntity.prototype.manage_mousemove_event = function(evt)
{
  this.mousePos = getMousePos(this.c, evt);
  return false;
}
    
CanvasEntity.prototype.manage_wheel_event = function(evt)
{
  this.mousePos = getMousePos(this.c, evt);
  return false;
}

CanvasEntity.prototype.inside_bounds = function(x, y)
{
  if (x >= this.offset_x && x < this.offset_x + this.width && y >= this.offset_y && y < this.offset_y + this.height)
    console.log("inside_bounds - "+x+','+y+','+this.width+','+this.height);
  else
    console.log("not inside_bounds - "+x+','+y+','+this.width+','+this.height);
  return x >= this.offset_x && x < this.offset_x + this.width && y >= this.offset_y && y < this.offset_y + this.height;
}
