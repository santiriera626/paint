"use strict";

var Matrix = function(offset_x, offset_y, num_cols, num_rows, color_palette, pen_palette) 
{
  CanvasEntity.call(this, c, offset_x, offset_y);

  this.num_cols = num_cols;
  this.num_rows = num_rows;
  
  this.color_palette = color_palette;
  this.pen_palette = pen_palette;
  
  this.width = this.num_cols * 8;
  this.height = this.num_rows * 8;

  this.init_matrix = function()
  {
    var matrix = [];
    for(var i=0; i<this.num_cols; i++) {
      matrix[i] = [];
      for(var j=0; j<this.num_rows; j++) {
          matrix[i][j] = "#FFFFFF";
      }
    }
    return matrix;
  };

  this.matrix = this.init_matrix();
}

inheritsFrom(Matrix, CanvasEntity);


Matrix.prototype.change_cells = function(i, j, evt)
{
  var color;
  console.log("Matrix.prototype.change_cells("+i+","+j+")");

  if ( i<0 || j<0 || i>=this.num_cols || j >= this.num_rows)
    return false;


  console.log("Matrix.prototype.change_cells2");
  if(evt.button === LEFT_BTN) 
    color = this.color_palette.colour;
  else if (evt.button === RIGHT_BTN) 
    color = this.color_palette.colour2;
  console.log("Matrix.prototype.change_cells3");

  return this.pen_palette.change_cells(i , j, color)
}


Matrix.prototype.manage_mousemove_event = function(evt)
{
  if (this.mouse_down) 
    console.log("Matrix.prototype.manage_mousemove_event MOUSE_DOWN");
  else
    console.log("Matrix.prototype.manage_mousemove_event");
  CanvasEntity.prototype.manage_mousemove_event.call(this, evt);
  if (this.mouse_down)
  {
    var cell = this.get_cell(this.mousePos.x, this.mousePos.y)
    if (cell!=false)
      return this.change_cells(cell[0], cell[1], evt);
  }
  return false;
}

Matrix.prototype.manage_mousedown_event = function(evt)
{
  console.log("Matrix.prototype.manage_mousedown_event");
  CanvasEntity.prototype.manage_mousedown_event.call(this, evt);
  if (this.mouse_down)
  {
    var cell = this.get_cell(this.mousePos.x, this.mousePos.y)
    if (cell!=false)
      return this.change_cells(cell[0], cell[1], evt);
  }
  return false;
}

Matrix.prototype.draw_cells = function(ctx)
{
  for(var i=0; i<this.num_cols; i++) {
    for(var j=0; j<this.num_rows; j++) {
      ctx.fillStyle = this.matrix[i][j];
      ctx.fillRect(i*this.width/this.num_cols,j*this.height/this.num_rows, this.width/this.num_cols, this.height/this.num_rows);
    }
  }
}

Matrix.prototype.draw_grid = function(ctx)
{
  ctx.beginPath();
  for(var i=0; i<this.num_cols+1; i++) {
    ctx.moveTo(Math.floor(i*this.width/this.num_cols), 0);
    ctx.lineTo(Math.floor(i*this.width/this.num_cols), this.height);
  }
  for(var j=0; j< this.num_rows+1; j++) {
    ctx.moveTo(0, Math.floor(j*this.height/this.num_rows));
    ctx.lineTo(this.width, Math.floor(j*this.height/this.num_rows));
  }
  ctx.strokeStyle = "#000000";
  ctx.stroke();
}

Matrix.prototype.get_cell = function(x, y)
{
  var i = Math.floor(x/(this.width/this.num_cols));
  var j = Math.floor(y/(this.height/this.num_rows));
  if ( i<0 || j<0 || i>=this.num_cols || j >= this.num_rows)
    return false;
  return [i,j];
}


Matrix.prototype.draw = function(ctx)
{
  this.draw_cells(ctx);
  this.draw_grid(ctx);
}


