"use strict";
var LEFT_BTN = 0;
var RIGHT_BTN = 2;

var DEBUG = true;

function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}


var inheritsFrom = function (child, parent) {
  child.prototype = Object.create(parent.prototype);
};