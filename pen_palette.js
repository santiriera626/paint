SQUARE_PEN = 0;
CIRCLE_PEN = 1;
SPRAY_PEN = 2;
CUBE_PEN = 3;

"use strict";

var PenPalette = function(c, offset_x, offset_y, matrix) {

  DragableEntity.call(this, c, offset_x, offset_y);
  

  this.num_cols = 1;
  this.num_rows = 4;
  this.width = 32;
  this.height = 128; 
  
  this.pens_list = [[SQUARE_PEN, CIRCLE_PEN, SPRAY_PEN, CUBE_PEN]];
  this.matrix = matrix;
  this.pen_type = SQUARE_PEN;
  this.pen_size = 1;

  this.offset_x = offset_x;
  this.offset_y = offset_y;

}

inheritsFrom(PenPalette, DragableEntity);


PenPalette.prototype.draw_cells = function(ctx)
{
  inner_width = this.width/this.num_cols-10;
  inner_height = this.height/this.num_rows-10;
  inner_offset_x = 5;
  inner_offset_y = 5;

  for(var i=0; i<this.num_cols; i++) {
    for(var j=0; j<this.num_rows; j++) {
      ctx.fillStyle = "#ffffff";
      if (this.pens_list[i][j] == SQUARE_PEN)
        ctx.fillRect(this.offset_x + inner_offset_x + i*this.width/this.num_cols,this.offset_y + inner_offset_y + j*this.height/this.num_rows, inner_width, inner_height);
      else if (this.pens_list[i][j] == CIRCLE_PEN)
      {
        ctx.beginPath();
        ctx.arc( this.offset_x+inner_offset_x+inner_width/2+i*this.width/this.num_cols, this.offset_y+inner_offset_y+ j*this.height/this.num_rows+inner_height/2, inner_width/2, 0, 2 * Math.PI, false);
        ctx.fill();
      }
      else if (this.pens_list[i][j] == CUBE_PEN)
        ctx.fillRect(this.offset_x + inner_offset_x + i*this.width/this.num_cols,this.offset_y + inner_offset_y + j*this.height/this.num_rows, inner_width, inner_height);
      else if (this.pens_list[i][j] == SPRAY_PEN)
      {
        ctx.fillStyle = "#000000";
        ctx.beginPath();
        ctx.arc( this.offset_x+inner_offset_x+inner_width/2+i*this.width/this.num_cols, this.offset_y+inner_offset_y+ j*this.height/this.num_rows+inner_height/2, inner_width/2, 0, 2 * Math.PI, false);
        ctx.fill();
      }      
    
}  }
}

PenPalette.prototype.change_cells = function(i, j, color)
{
  var changed = false;
  console.log("PenPalette.prototype.change_cells00");
  if (this.pen_type === CUBE_PEN )
  {
    return this.change_cells_flood(i, j, color);
  }
  else if (this.pen_type === CIRCLE_PEN || this.pen_type === SQUARE_PEN || this.pen_type === SPRAY_PEN)
  {
    var random = 1;
    if (this.pen_type == SPRAY_PEN)
      random = 0.2;
    l = this.get_affected_cells( i, j);
    for(var ii=0; ii<l.length; ii++)
      if (this.matrix[l[ii][0]][l[ii][1]]!= color && Math.random()<=random)
      {
        this.matrix[l[ii][0]][l[ii][1]] = color;
        changed=true;
      }
  }

  return changed;
}

PenPalette.prototype.get_affected_cells = function( i, j)
{
  var l = [];
  if (this.pen_type === CIRCLE_PEN || this.pen_type === SPRAY_PEN )
    for(var ii=Math.round(i-this.pen_size/2); ii<Math.round(i+this.pen_size-this.pen_size/2); ii++) {
      for(var jj=Math.round(j-this.pen_size/2); jj<Math.round(j+this.pen_size-this.pen_size/2); jj++) {
        if (ii<0 || jj<0 || ii>this.matrix.length-1 || jj> this.matrix[0].length-1)
          continue;
        var x = ii-i;
        var y = jj-j;
      
        if (Math.sqrt( x*x + y*y) < this.pen_size/2)
          l.push([ii,jj]);
      }
    }
  else if (this.pen_type === SQUARE_PEN )
    for(var ii=i; ii<i+this.pen_size; ii++) {
      for(var jj=j; jj<j+this.pen_size; jj++) {
        if (ii<0 || jj<0 || ii>this.matrix.length-1 || jj> this.matrix[0].length-1)
          continue;
        l.push([ii,jj]);
      }
    }
  return l;
}


PenPalette.prototype.change_cells_flood = function(i, j, color)
{
  var visited = [];
  var back_color = this.matrix[i][j];

  for(var ii=0; ii<this.matrix.length; ii++) {
    visited[ii] = [];
    for(var jj=0; jj<this.matrix[0].length; jj++) {
      visited[ii][jj] = false;
    }
  }

  var list = []
  list.push([i,j]);

  while (list.length > 0)
  {
    var cell = list.pop();
    
    if (cell[0]<0 || cell[1]<0 || cell[0]>this.matrix.length-1 || cell[1]> this.matrix[0].length-1 || visited[cell[0]][cell[1]] )
      continue;
    
    visited[cell[0]][cell[1]] = true;
    
    if (this.matrix[cell[0]][cell[1]].valueOf()  == back_color.valueOf() ){
      this.matrix[cell[0]][cell[1]] = color;
    
      //123
      //456
      //789
      list.push([cell[0],cell[1]-1]); //2
      list.push([cell[0]-1,cell[1]]); //4
      list.push([cell[0]+1,cell[1]]); //6
      list.push([cell[0],cell[1]+1]); //8
    }

  }
  return true;
}

PenPalette.prototype.draw_grid = function(ctx)
{
  ctx.beginPath();
  ctx.strokeStyle = "#000000";
  for(var i=0; i<this.num_cols+1; i++) {
    ctx.moveTo(Math.floor(this.offset_x + i*this.width/this.num_cols), this.offset_y);
    ctx.lineTo(Math.floor(this.offset_x + i*this.width/this.num_cols), this.offset_y + this.height);
  }
  for(var j=0; j<this.num_rows+1; j++) {
    ctx.moveTo(this.offset_x + 0, Math.floor(this.offset_y + j*this.height/this.num_rows));
    ctx.lineTo(this.offset_x + this.width, Math.floor( this.offset_y + j*this.height/this.num_rows));
  }
  ctx.stroke();
}



PenPalette.prototype.draw_pencil_shadow = function(ctx, i, j)
{
  
  ctx.fillStyle = "#000000";
  l = this.get_affected_cells( i, j);
  for(var ii=0; ii<l.length; ii++)
    ctx.fillRect( l[ii][0]*this.matrix.width/this.matrix.num_cols, l[ii][1]*this.matrix.height/this.matrix.num_rows, this.matrix.width/this.matrix.num_cols, this.matrix.height/this.matrix.num_rows);
}

PenPalette.prototype.draw = function(ctx)
{
  
  DragableEntity.prototype.draw.call(this, ctx);
  this.draw_cells(ctx);
  this.draw_grid(ctx);

}

PenPalette.prototype.manage_mousemove_event = function(evt)
{
  return DragableEntity.prototype.manage_mousemove_event.call(this, evt);
}

PenPalette.prototype.manage_mousedown_event = function(evt)
{
 
  DragableEntity.prototype.manage_mousedown_event.call(this, evt)
  if (this.mouse_down)
  {
    var i = Math.floor((this.mousePos.x - this.offset_x)/(this.width/this.num_cols));
    var j = Math.floor((this.mousePos.y - this.offset_y)/(this.height/this.num_rows));

    if (i<0 || j<0 || i>this.num_cols-1 || j>this.num_rows-1)
      return false;
    this.pen_type = this.pens_list[i][j];

    return true;
  }
  return false;
}

PenPalette.prototype.manage_wheel_event = function(evt)
{
  if (evt.wheelDelta<0)
    this.pen_size = Math.max(1, this.pen_size-1);
  else if (evt.wheelDelta>0)
    this.pen_size = Math.min(20, this.pen_size+1);
  return true;
}

