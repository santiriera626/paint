"use strict";

var DragableEntity = function( c, offset_x, offset_y)
{
  CanvasEntity.call(this, c, offset_x, offset_y);
  this.width = 40;

  this.mouse_drag = false;    
  this.drag_point = {
    x: 0,
    y: 0
  };
}

inheritsFrom(DragableEntity, CanvasEntity);


DragableEntity.prototype.draw = function(ctx) 
{
  CanvasEntity.prototype.draw.call(this, ctx);
  if (this.mouse_drag)
  {
    ctx.beginPath();
    ctx.strokeStyle = "#00ff00";
    ctx.rect(this.offset_x-1, this.offset_y-1,this.width+2,this.height+2);
    ctx.stroke();
  }
}

DragableEntity.prototype.manage_mousedown_event = function(evt)
{
  CanvasEntity.prototype.manage_mousedown_event.call(this, evt);
  if (this.mouse_down)
  {
    this.mouse_drag=true;
    this.drag_point = {
      x: this.mousePos.x - this.offset_x,
      y: this.mousePos.y - this.offset_y
    };
    return true;
  }
  return false;
}

DragableEntity.prototype.manage_mouseup_event = function(evt)
{
  CanvasEntity.prototype.manage_mouseup_event.call(this, evt);
  this.mouse_drag = false;

  return false;
}

DragableEntity.prototype.manage_mousemove_event = function(evt)
{
  CanvasEntity.prototype.manage_mousemove_event.call(this, evt)
  if (this.mouse_drag)
  {
      var new_offset_x =  this.mousePos.x - this.drag_point.x;
      var new_offset_y =  this.mousePos.y - this.drag_point.y;
      
      if (new_offset_x<0)
        new_offset_x = 0;
      else if (new_offset_x + this.width > this.c.width)
        new_offset_x = this.c.width - this.width;

      if (new_offset_y<0)
        new_offset_y = 0;
      else if (new_offset_y + this.height > this.c.height)
        new_offset_y = this.c.height - this.height;

      this.offset_x =new_offset_x;
      this.offset_y = new_offset_y; 
      return true;
    }
    return false;
  }
